import * as TypesFirebaseAdmin from '../types/firebase-admin-types';
declare class FirebaseAdmin {
    private static readonly admin;
    private static readonly log;
    private static readonly cloudMessaging;
    static messagingToDevice(tokens: any[] | TypesFirebaseAdmin.TokenObject, data: any, opts?: TypesFirebaseAdmin.OptionMessageToDevice): void;
}
export default FirebaseAdmin;
