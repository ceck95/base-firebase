"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/*
 * @Author: Tran Van Nhut (nhutdev)
 * @Date: 2017-11-22 14:32:26
 * @Last Modified by: Tran Van Nhut (nhutdev)
 * @Last Modified time: 2017-11-23 09:32:22
 */
const config = require('config');
const admin = require('firebase-admin');
const Hoek = require('hoek');
const BPromise = require('bluebird');
const Log = require('core-express').Log;
class FirebaseAdmin {
    static get admin() {
        let configFirebase = null;
        try {
            configFirebase = require(`${process.cwd()}/config/firebase.json`);
        }
        catch (error) {
            Hoek.assert(config.firebase, 'Must be null config firebase');
            configFirebase = config.firebase;
        }
        if (admin.apps.length) {
            return admin;
        }
        return admin.initializeApp({
            credential: admin.credential.cert(configFirebase)
        });
    }
    static get log() {
        return Log;
    }
    static get cloudMessaging() {
        return FirebaseAdmin.admin.messaging();
    }
    static messagingToDevice(tokens, data, opts = {
            keyToken: null
        }) {
        Hoek.assert(tokens && data, 'Tokens and content cannot null');
        if (Array.isArray(tokens)) {
            tokens = tokens.map(e => {
                let current = e[opts.keyToken || 'deviceId'];
                if (!Array.isArray(current)) {
                    try {
                        current = JSON.parse(current);
                    }
                    catch (error) {
                        current = [];
                    }
                }
                return current;
            }).filter(e => {
                if (e.length === 0)
                    return false;
                return true;
            });
            tokens = tokens.reduce((a, b) => {
                return a.concat(b);
            }, []);
        }
        else if (typeof tokens === 'object') {
            const current = tokens[opts.keyToken || 'deviceId'];
            try {
                tokens = JSON.parse(current);
            }
            catch (error) {
                tokens = [];
            }
        }
        const payload = {
            notification: {
                title: data.title,
                body: data.body
            }
        };
        const self = FirebaseAdmin;
        self.log.info('Argument data to sent notification');
        self.log.info({
            data: {
                tokens: tokens,
                payload: data
            }
        });
        if (Array.isArray(tokens) && tokens.length === 0) {
            self.log.error('Tokens must have');
            return BPromise.resolve(true);
        }
        return self.cloudMessaging.sendToDevice(tokens, payload).then((response) => {
            self.log.info('[Cloud messaging] sent notification to device successfully');
            return BPromise.resolve(response);
        }).catch((err) => {
            self.log.error('[Cloud messaging] sent notification to device fail');
            return BPromise.reject(err);
        });
    }
}
exports.default = FirebaseAdmin;
