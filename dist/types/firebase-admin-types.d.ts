export interface OptionMessageToDevice {
    keyToken: string;
}
export interface TokenObject {
    [key: string]: string;
}
